
function myFunction(id){
  var element = document.getElementById(id);
  var children = element.childNodes;
  for (var i = 0; i < children.length; i++){
    $("#" + id).children(i).toggle();
  }
}

//Enabling navigation for mobile screen
function mobileNav() {
    var x = document.getElementById("mynav");
    if (x.className === "mainNav") {
        x.className += " responsive";
        $(".content").hide();
    } else {
        x.className = "mainNav";
        $(".content").show();
    }
}

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("topbutton").style.display = "block";
    } else {
        document.getElementById("topbutton").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

$(document).ready(function(){

//enabling the toggling of display boxes on the home page
  $(".grid-item").hover(function(){
      myFunction(this.id);
      }, function(){
      myFunction(this.id);
  });

$(".templateBox").click(function(){
  myFunction(this.id);
  }, function(){
  myFunction(this.id);
});

window.onscroll = function() {scrollFunction()};
});
